public class IRInfo {

    private int statement;
    private String filename;

    public IRInfo(){
        this.statement = -1;
        this.filename = "";
    }

    public IRInfo(int line, String fname){
        this.statement = line;
        this.filename = fname;
    }

    public int getStatement(){
        return this.statement;
    }

    public void setStatement(int tstate){
        this.statement = tstate;
    }

    public String getFilename(){
        return this.filename;
    }

    public void setFilename(String fname2){
        this.filename = fname2;
    }
}
