import com.intellij.ide.plugins.IdeaPluginDescriptor;
import com.intellij.ide.plugins.PluginManager;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.event.EditorMouseMotionListener;
import com.intellij.openapi.extensions.PluginId;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.util.TextRange;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowManager;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import javafx.util.Pair;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.regex.Pattern;

public class MainMenu {
    AnActionEvent a;
    public JFrame frame = new JFrame("PowerStation");
    public JPanel rootComponent = new JPanel();
    public JButton IRbutton = new JButton("IR");
    public JButton commonExpButton= new JButton("CS");
    public JButton ineffAPIbutton = new JButton("IA");
    public JButton loopInvButton = new JButton("LI");
    public JButton deadstoreButton = new JButton("DS");
    public JButton redundantDataButton = new JButton("RD");
    public JPanel IRPanel = new JPanel();
    public JPanel CSPanel = new JPanel();
    public JPanel IAPanel = new JPanel();
    public JPanel LIPanel = new JPanel();
    public JPanel DSPanel = new JPanel();
    public JPanel RDPanel = new JPanel();
    public JPanel IRFilePanel = new JPanel();
    public JPanel CSFilePanel = new JPanel();
    public JPanel IAFilePanel = new JPanel();
    public JPanel LIFilePanel = new JPanel();
    public JPanel DSFilePanel = new JPanel();
    public JPanel RDFilePanel = new JPanel();
    public JPanel ButtonPanel = new JPanel();

    private boolean full;
    private boolean Lfull;
    private boolean Dfull;
    private boolean Ifull;

    public MainMenu(AnActionEvent a){
        this.a = a;
        this.full = false;
        this.Lfull = false;
        this.Dfull = false;
        this.Ifull = false;
        String basedir = this.a.getProject().getBasePath();
        if (basedir == null){
            System.out.println("ERROR: basedir is null");
            System.exit(-1);
        }else {
            System.out.println(basedir);
        }

        IdeaPluginDescriptor plugin = PluginManager.getPlugin(PluginId.getId("com.intellij.powerstation"));
        String powerstation_path = plugin.getPath().toString();
        String lib = "/";
        String root  = "static-analyzer/";
        String input = a.getProject().getBasePath();
        String[] appnames = input.split("/");
        String path_ana = powerstation_path + lib + root;
        String app = "PW-" + appnames[appnames.length - 1];
        String applications = path_ana + "/applications";
        String results_path =  applications + "/" + app + "/results/";
        results_path = Util.getRP(a.getProject());

        ButtonPanel.setLayout(new GridLayout(0,6));
        this.ineffAPIbutton.setPreferredSize(new Dimension(60, 20));
        this.commonExpButton.setPreferredSize(new Dimension(60, 20));
        this.IRbutton.setPreferredSize(new Dimension(60, 20));
        this.deadstoreButton.setPreferredSize(new Dimension(60, 20));
        this.loopInvButton.setPreferredSize(new Dimension(60, 20));
        this.redundantDataButton.setPreferredSize(new Dimension(60, 20));
        ButtonPanel.add(this.ineffAPIbutton);
        ButtonPanel.add(this.commonExpButton);
        ButtonPanel.add(this.IRbutton);
        ButtonPanel.add(this.deadstoreButton);
        ButtonPanel.add(this.loopInvButton);
        ButtonPanel.add(this.redundantDataButton);
        ButtonPanel.setSize(ButtonPanel.getWidth(), 20);
        //rootComponent.add(ButtonPanel);
        rootComponent.setLayout(new FlowLayout());
        File file;


        IRPanel.setLayout(new GridLayout(2,0));
        IRbutton.setToolTipText("Inefficient Rendering");
        file = new File(results_path + "/inefficient_render.xml");
        //file = new File("/Users/jwy/Research/Code/pw/src/main/resources/static-analyzer/applications/pw-blog/results/inefficient_render.xml");
        List<IRInfo> irInfos = Util.extractIRInfo(file);
        List<String> IRfs = Util.constructIRFilePanel(irInfos);
        List<JButton> IRLabels = Util.constructFilesPanel(IRfs);
        IRFilePanel = Util.constructFiles(IRLabels);
        IRPanel.add(IRFilePanel);

        CSPanel.setLayout(new GridLayout(2,0));
        CSPanel.setToolTipText("Queries with common subexpressions");
        file = new File(results_path + "/commmon_subexpression.xml");
        //file = new File("/Users/jwy/Research/Code/pw/src/main/resources/static-analyzer/applications/pw-blog/results/commmon_subexpression.xml");
        List<CSInfo> csinfos = Util.extractCSInfo(file);
        List<String> CSfs = Util.constructCSFilePanel(csinfos);
        List<JButton> CSLabels = Util.constructFilesPanel(CSfs);
        CSFilePanel = Util.constructFiles(CSLabels);
        CSPanel.add(CSFilePanel);
        CSFilePanel.setVisible(true);


        IAPanel.setLayout(new GridLayout(2,0));
        IAPanel.add(IAFilePanel);
        ineffAPIbutton.setToolTipText("Inefficient API");
        JButton cufBtn = new JButton("Current File");
        IRFilePanel.setLayout(new FlowLayout());
        cufBtn.setPreferredSize(new Dimension(200, 20));
        IAFilePanel.add(cufBtn);

        LIPanel.setLayout(new GridLayout(2,0));
        loopInvButton.setToolTipText("Loop Invariant Expressions");
        file = new File(results_path + "/loop_invariant.xml");
        //file = new File("/Users/jwy/Research/Code/pw/src/main/resources/static-analyzer/applications/pw-blog/results/loop_invariant.xml");
        List<LoopInfo> loopInfos = Util.extractLIInfo(file);
        List<String> LIfs = Util.constructLIFilePanel(loopInfos);
        List<JButton> LILabels = Util.constructFilesPanel(LIfs);
        LIFilePanel = Util.constructFiles(LILabels);
        LIPanel.add(LIFilePanel);
        LIFilePanel.setVisible(true);

        DSPanel.setLayout(new GridLayout(2,0));
        deadstoreButton.setToolTipText("Dead Store Queries");
        file = new File(results_path + "/dead_store.xml");
        //file = new File("/Users/jwy/Research/Code/pw/src/main/resources/static-analyzer/applications/pw-blog/results/dead_store.xml");
        List<DSInfo> dsInfos = Util.extractDSInfo(file);
        List<String> DSfs = Util.constructDSFilePanel(dsInfos);
        List<JButton> DSLabels = Util.constructFilesPanel(DSfs);
        DSFilePanel = Util.constructFiles(DSLabels);
        DSPanel.add(DSFilePanel);
        DSFilePanel.setVisible(true);
        DSPanel.setVisible(true);

        RDPanel.setLayout(new GridLayout(2,0));
        redundantDataButton.setToolTipText("Redundant Data Retrieval");
        file = new File(results_path + "/redundant_usage.xml");
        //file = new File("/Users/jwy/Research/Code/pw/src/main/resources/static-analyzer/applications/pw-blog/results/redundant_usage.xml");
        List<RDInfo> rdinfos = Util.extractRDInfos(file);
        List<String> fs = Util.extractFiles(rdinfos);
        List<JButton> RDLabels = Util.constructFilesPanel(fs);
        RDFilePanel = Util.constructFiles(RDLabels);
        RDPanel.add(RDFilePanel);

        JTabbedPane tp = new JTabbedPane();
        tp.add("LI", LIPanel);
        tp.setToolTipTextAt(0, "Loop Invariant");
        tp.add("IA", IAPanel);
        tp.setToolTipTextAt(1, "Inefficient API");
        tp.add("CS", CSPanel);
        tp.setToolTipTextAt(2, "Common Subexpressions");
        tp.add("IR", IRPanel);
        tp.setToolTipTextAt(3, "Inefficient Rendering");
        tp.add("DS", DSPanel);
        tp.setToolTipTextAt(4, "Dead Store queries");
        tp.add("RD", RDPanel);
        tp.setToolTipTextAt(5, "Redundant Data Retrieval");
        rootComponent.add(tp);

        tp.addChangeListener(new ChangeListener() { //add the Listener

            public void stateChanged(ChangeEvent e) {

                System.out.println(""+tp.getSelectedIndex());

                if(tp.getSelectedIndex()==0)
                {
                    LoopInvHandler lih = new LoopInvHandler();
                    lih.actionPerformed(a);


                }
                if(tp.getSelectedIndex()==1)
                {
                    IneffAPIHandler h = new IneffAPIHandler();
                    h.actionPerformed(a);
                }
                if(tp.getSelectedIndex()==2)
                {
                    CSHandler csh = new CSHandler();
                    csh.actionPerformed(a);
                }
                if(tp.getSelectedIndex()==3)
                {
                    IRHandler irh = new IRHandler();
                    irh.actionPerformed(a);

                }
                if(tp.getSelectedIndex()==4)
                {
                    DSQHandler dqh = new DSQHandler();
                    dqh.actionPerformed(a);
                }
                if(tp.getSelectedIndex()==5)
                {
                    RDHandler rdh = new RDHandler();
                    rdh.actionPerformed(a);
                }
            }
        });
        for(int i = 0 ;i < IRLabels.size(); i ++){
            JButton label = IRLabels.get(i);
            label.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Util.removeHighlighters(a.getProject());
                    String view_file_path = basedir + "/app/" + label.getText();
                    System.out.println(view_file_path);
                    VirtualFile view_file = LocalFileSystem.getInstance().findFileByPath(view_file_path);
                    FileEditorManager.getInstance(a.getProject()).openFile(view_file, true);
                    IRHandler irh = new IRHandler();
                    irh.actionPerformed(a);
                }
            });

        }
        IRbutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IRHandler irh = new IRHandler();
                irh.actionPerformed(a);
            }


        });


        for(int i = 0 ;i < CSLabels.size(); i ++){
            JButton label = CSLabels.get(i);
            label.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    Util.removeHighlighters(a.getProject());
                    String view_file_path = basedir + "/app/" + label.getText();
                    System.out.println(view_file_path);
                    VirtualFile view_file = LocalFileSystem.getInstance().findFileByPath(view_file_path);
                    FileEditorManager.getInstance(a.getProject()).openFile(view_file, true);
                    CSHandler csh = new CSHandler();
                    csh.actionPerformed(a);
                }
            });

        }
        commonExpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CSHandler csh = new CSHandler();
                csh.actionPerformed(a);

            }


        });

        for(int i = 0 ;i < RDLabels.size(); i ++){
            JButton label = RDLabels.get(i);
            label.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    Util.removeHighlighters(a.getProject());
                    String view_file_path = basedir + "/app/" + label.getText();
                    System.out.println(view_file_path);
                    VirtualFile view_file = LocalFileSystem.getInstance().findFileByPath(view_file_path);
                    FileEditorManager.getInstance(a.getProject()).openFile(view_file, true);
                    RDHandler rdh = new RDHandler();
                    rdh.actionPerformed(a);

                }
            });

        }
        redundantDataButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                RDHandler rdh = new RDHandler();
                rdh.actionPerformed(a);

            }


        });

        ineffAPIbutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                IneffAPIHandler h = new IneffAPIHandler();
                h.actionPerformed(a);

            }


        });

        cufBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                IneffAPIHandler h = new IneffAPIHandler();
                h.actionPerformed(a);

            }


        });

        for(int i = 0 ;i < LILabels.size(); i ++){
            JButton label = LILabels.get(i);
            label.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    Util.removeHighlighters(a.getProject());
                    String view_file_path = basedir + "/app/" + label.getText();
                    System.out.println(view_file_path);
                    VirtualFile view_file = LocalFileSystem.getInstance().findFileByPath(view_file_path);
                    FileEditorManager.getInstance(a.getProject()).openFile(view_file, true);
                    LoopInvHandler lih = new LoopInvHandler();
                    lih.actionPerformed(a);
                }
            });

        }
        loopInvButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Loop Inva : " + LILabels.size());
                LoopInvHandler lih = new LoopInvHandler();
                lih.actionPerformed(a);

            }


        });

        for(int i = 0 ;i < DSLabels.size(); i ++){
            JButton label = DSLabels.get(i);
            label.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    Util.removeHighlighters(a.getProject());
                    String view_file_path = basedir + "/app/" + label.getText();
                    System.out.println(view_file_path);
                    VirtualFile view_file = LocalFileSystem.getInstance().findFileByPath(view_file_path);
                    FileEditorManager.getInstance(a.getProject()).openFile(view_file, true);
                    DSQHandler dqh = new DSQHandler();
                    dqh.actionPerformed(a);
                }
            });

        }

        deadstoreButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DSQHandler dqh = new DSQHandler();
                dqh.actionPerformed(a);
            }


        });


    }


}
