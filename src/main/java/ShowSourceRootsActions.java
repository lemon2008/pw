import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;
import java.io.IOException;
import com.intellij.openapi.*;
import com.intellij.ide.plugins.*;
import com.intellij.openapi.extensions.PluginId;
import java.io.File;

public class ShowSourceRootsActions extends AnAction {
    String results_path = "";
    @Override
    public void actionPerformed(@NotNull final AnActionEvent event) {
        Project project = event.getProject();
        if (project == null) return;
        String projectName = project.getName();
        StringBuilder sourceRootsList = new StringBuilder();
        VirtualFile[] vFiles = ProjectRootManager.getInstance(project).getContentSourceRoots();
        for (VirtualFile file : vFiles) {
            sourceRootsList.append(file.getUrl()).append("\n");

        }
        //System.out.println("current work directory is : " + PluginManager.getPlugin(PluginId.getId("com.intellij.powerstation")).getPath() );
        //System.out.println(PathManager.getPluginsPath());
        String lib = "/";
        String root  = "static-analyzer/";
        String pre = "preprocess_views/";
        String application = "applications";
        IdeaPluginDescriptor plugin = PluginManager.getPlugin(PluginId.getId("com.intellij.powerstation"));
        String powerstation_path = plugin.getPath().toString();
        // get the version of the plugin in order to get the snapshot's name
        String version = plugin.getVersion();
        String name = plugin.getName();
        String snapshot = name + "-" + version + ".jar";
        powerstation_path = powerstation_path.replace(snapshot, "");
        System.out.println("path of powerstation is " + powerstation_path);
        System.out.println("snapshot name is :" + snapshot);

        String path_lib = powerstation_path + lib;
        String path_ana = powerstation_path + lib + root; //static-analyzer/analyze.sh
        String script_ana_name = "analyze.sh";
        String authorize = "chmod +x ";
        String authorize_ana = authorize + script_ana_name;
        String applications = path_ana + "/applications";
        String path_extract_ruby = path_ana + "/" + pre;
        String script_extract_ruby_name = "extract_ruby";
        String authorize_extract_ruby = authorize + script_extract_ruby_name;

        String script = "./" + script_ana_name;
        String basedir = event.getProject().getBasePath();
        String input = "/home/junwen/research/Code/blog/"; // TD: read from editor
        input = basedir;
        String[] appnames = input.split("/");
        String app = "PW-" + appnames[appnames.length - 1];
        //String app = "pw-blog"; //customize with user app name
        String run_script = script + " " + app + " " + input;
        String result_path = path_ana + "finish.txt";
        String unzip =  "jar xf " + snapshot;
        results_path =  applications + "/" + app + "/results/";
        Messages.showInfoMessage("Will analyze " + input + "\n Powerstation path" + powerstation_path + "\n Results_path:" + results_path,
                "Project Properties");
        try {
            // unzip the jar file to get the shell and other files
            //Analyze.runCommand("/home/junwen/", jruby);

            RunShell.runCommand(path_lib, unzip);
            // authorize analyze sh and the extract ruby file
            RunShell.runCommand(path_ana, authorize_ana);

            RunShell.runCommand(path_extract_ruby, authorize_extract_ruby);

            // analysis
            File f = new File(result_path);
            //Analyze.runCommand(path_ana, jruby);
            if(!f.exists())
                RunShell.runCommand(path_ana, run_script);
        }
        catch(IOException e){
            System.out.println("IO EXCEPTION");
        }
        catch(InterruptedException ie){
            System.out.println("InterruptedException happened");
        }
    }

    @Override
    public void update(@NotNull final AnActionEvent event) {
        boolean visibility = event.getProject() != null;
        event.getPresentation().setEnabled(visibility);
        event.getPresentation().setVisible(visibility);
    }
}