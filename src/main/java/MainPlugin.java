import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowManager;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;

import javax.swing.*;
import java.awt.*;

public class MainPlugin extends AnAction {

    public void actionPerformed(AnActionEvent e) {
        //Want to make a menu for the plugin and then input the existing code
        Project project = e.getProject();
        actionPerformed(e, project, SettingsContext.NONE);
    }

    public void actionPerformed(AnActionEvent e, Project project, SettingsContext settingsContext){
        MainMenu mainMenu = new MainMenu(e);
        //JLabel perflabel = new JLabel();
        //perflabel.setFont(new Font("Verdana", Font.PLAIN, 14));
        //mainMenu.rootComponent.setLayout(new BoxLayout(mainMenu.rootComponent, BoxLayout.PAGE_AXIS));
        //mainMenu.rootComponent.setLayout(new GridLayout(0,1));
        //mainMenu.rootComponent.add(perflabel);
        //perflabel.setAlignmentX(Component.CENTER_ALIGNMENT);
//        mainMenu.rootComponent.add(mainMenu.ineffAPIbutton);
//        //mainMenu.ineffAPIbutton.setAlignmentX(Component.CENTER_ALIGNMENT);
//        //mainMenu.ineffAPIbutton.setHorizontalAlignment(SwingConstants.CENTER);
//        mainMenu.rootComponent.add(mainMenu.commonExpButton);
//        //mainMenu.pagbutton.setAlignmentX(Component.CENTER_ALIGNMENT);
//        mainMenu.rootComponent.add(mainMenu.IRbutton);
//        //mainMenu.IRbutton.setAlignmentX(Component.CENTER_ALIGNMENT);
//        mainMenu.rootComponent.add(mainMenu.loopInvButton);
//        //mainMenu.loopInvButton.setAlignmentX(Component.CENTER_ALIGNMENT);
//        mainMenu.rootComponent.add(mainMenu.deadstoreButton);
//        //mainMenu.deadstoreButton.setAlignmentX(Component.CENTER_ALIGNMENT);
//        mainMenu.rootComponent.add(mainMenu.redundantDataButton);
        //mainMenu.rootComponent.add(mainMenu.ButtonPanel);

        mainMenu.frame.add(mainMenu.rootComponent);
        Dimension dim  = Toolkit.getDefaultToolkit().getScreenSize();
        mainMenu.frame.setLocation(dim.width/2-mainMenu.frame.getSize().width/2, dim.height/2-mainMenu.frame.getSize().height/2);
        mainMenu.frame.pack();
        //mainMenu.frame.setVisible(true);

        final ToolWindow refactor_window = ToolWindowManager.getInstance(project).getToolWindow("powerstation");
        refactor_window.activate(new Runnable() {
            @Override
            public void run() {
                ContentFactory contentFactory = ContentFactory.SERVICE.getInstance();
                Content content = contentFactory.createContent(mainMenu.rootComponent, "", false);
                refactor_window.getContentManager().addContent(content);
                System.out.println("Added Content");
            }
        });

    }
}
