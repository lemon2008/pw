import com.intellij.ide.plugins.IdeaPluginDescriptor;
import com.intellij.ide.plugins.PluginManager;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.event.EditorMouseEvent;
import com.intellij.openapi.editor.event.EditorMouseMotionListener;
import com.intellij.openapi.extensions.PluginId;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.ui.awt.RelativePoint;
import com.intellij.ui.components.JBScrollPane;
import javafx.util.Pair;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Util {

    public static List<Pair<Editor, EditorMouseMotionListener>> editorListeners = new ArrayList<>();

    public static Pair<Editor, EditorMouseMotionListener> addTooltips(Editor editor, String error_msg, int loc){
        int height = editor.getLineHeight() * loc;
        Point p = new Point(200, height);
        com.intellij.openapi.editor.Document d = editor.getDocument();
        int indlend = d.getLineEndOffset(loc - 1);
        int indlstart = d.getLineStartOffset(loc - 1);
        Point start = editor.offsetToXY(indlstart);
        Point end = editor.offsetToXY(indlend);
        double x1 = 0; //start.getX();
        double x2 = 300;// end.getX();
        double y1 = height - editor.getLineHeight();
        //double y1 = start.getY();
        double y2 = height;
        //double y2 = y1 + editor.getLineHeight();
        RelativePoint rp = new RelativePoint(editor.getContentComponent(), p);
        JLabel error_label = new JLabel(error_msg);
        JWindow jf = new JWindow();
        jf.setLayout(new BorderLayout());
        jf.add(error_label, BorderLayout.CENTER);
        jf.setLocationRelativeTo(editor.getContentComponent());
        jf.setLocation(rp.getPoint());
        jf.setSize(error_label.getPreferredSize());
        EditorMouseMotionListener emml = new EditorMouseMotionListener() {
            @Override
            public void mouseMoved(EditorMouseEvent e) {
                int x = e.getMouseEvent().getX();
                int y = e.getMouseEvent().getY();
                if(x > x1 && x < x2 && y > y1 && y < y2){
                    jf.setVisible(true);
                }
                else{
                    jf.setVisible(false);
                }
            }

            @Override
            public void mouseDragged(EditorMouseEvent e) {

            }
        };
        editor.addEditorMouseMotionListener(emml);
        Pair<Editor, EditorMouseMotionListener> pair =  new Pair<>(editor, emml);
        if(!editorListeners.contains(pair))
            editorListeners.add(pair);
        return new Pair<>(editor, emml);
    }
    public static void removeEditorListeners(List<Pair<Editor, EditorMouseMotionListener>> editorListeners) {
        for (Pair<Editor, EditorMouseMotionListener> pair : editorListeners){
            try {
                pair.getKey().removeEditorMouseMotionListener(pair.getValue());
            } catch (Throwable e) {

            }

        }
       editorListeners = new ArrayList<Pair<Editor, EditorMouseMotionListener>>();

    }
    public static List<CSInfo> extractCSInfo(File file){
        List<CSInfo> csinfos = new ArrayList<CSInfo>();
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            //normalize, so that text doesn't get parsed into separate nodes
            doc.getDocumentElement().normalize();
            //System.out.println(doc);
            System.out.println("Root element: " + doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName("subexprs");

            //LinkedList<DSInfo> dsInfo = new LinkedList<DSInfo>();
            System.out.println(nList.getLength());
            for (int temp = 0; temp < nList.getLength(); temp++) {
                CSInfo csinfo;
                Node nNode = nList.item(temp);

                System.out.println("\nCurrent Element :" + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    //System.out.println("Staff id : " + eElement.getAttribute("id"));
                    String expression = eElement.getElementsByTagName("subexpr").item(0).getTextContent();
                    NodeList exprsList = eElement.getElementsByTagName("node");
                    List<Statement> stms = new ArrayList<Statement>();
                    for (int i = 0; i < exprsList.getLength(); i++) {
                        Node node = exprsList.item(i);
                        if (node.getNodeType() == Node.ELEMENT_NODE) {

                            Element statement = (Element) node;
                            Statement stm;
                            String filename = statement.getElementsByTagName("filename").item(0).getTextContent();
                            ;
                            int loc = Integer.parseInt(statement.getElementsByTagName("loc").item(0).getTextContent());
                            stm = new Statement(loc, filename);
                            stms.add(stm);
                            System.out.println("filename: " + filename);
                            System.out.println("loc: " + loc);
                        }
                    }
                    if (stms.size() >= 2) {
                        csinfo = new CSInfo(expression, stms);
                        csinfos.add(csinfo);
                    }

                }
            }
            return csinfos;


        }catch (Exception ex) {
            System.out.println("Weird stuff happening..." + ex);
            Messages.showMessageDialog("Weird stuff happening..." + ex, "Info Box", Messages.getInformationIcon());
            return null;
        }

    }

    public static List<DSInfo> extractDSInfo(File file){
        List<DSInfo> dsinfos = new ArrayList<DSInfo>();
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            //normalize, so that text doesn't get parsed into separate nodes
            doc.getDocumentElement().normalize();
            //System.out.println(doc);
            System.out.println("Root element: " + doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName("dead_store_pairs");

            //LinkedList<DSInfo> dsInfo = new LinkedList<DSInfo>();
            System.out.println(nList.getLength());
            for (int temp = 0; temp < nList.getLength(); temp++) {
                DSInfo dsinfo;
                Node nNode = nList.item(temp);

                System.out.println("\nCurrent Element :" + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;
                    List<Statement> stms = new ArrayList<Statement>();
                    //System.out.println("Staff id : " + eElement.getAttribute("id"));
                    NodeList exprsList = eElement.getElementsByTagName("node");
                    for (int i = 0; i < exprsList.getLength(); i++) {
                        Node node = exprsList.item(i);
                        if (node.getNodeType() == Node.ELEMENT_NODE) {

                            Element statement = (Element) node;
                            Statement stm;
                            String filename = statement.getElementsByTagName("filename").item(0).getTextContent().trim();
                            int loc = Integer.parseInt(statement.getElementsByTagName("loc").item(0).getTextContent().trim());
                            stm = new Statement(loc, filename);
                            System.out.println("filename: " + filename);
                            System.out.println("loc: " + loc);
                            stms.add(stm);
                        }
                    }
                    if (stms.size() == 2) {
                        int loc1 = stms.get(0).getStatement();
                        int loc2 = stms.get(1).getStatement();
                        String filename1 = stms.get(0).getFilename();
                        String filename2 = stms.get(1).getFilename();
                        dsinfo = new DSInfo(loc1, loc2, filename1);
                        dsinfo.setFilename2(filename2);
                        dsinfos.add(dsinfo);
                    }

                }
            }



        }catch (Exception ex) {
            System.out.println("Weird stuff happening..." + ex);
            Messages.showMessageDialog("Weird stuff happening..." + ex, "Info Box", Messages.getInformationIcon());
            return null;
        }

        System.out.print("dsinfo"+ dsinfos.size());

        return dsinfos;
    }
    public static List<RDInfo> extractRDInfos(File file){
        List<RDInfo> rdinfos = new ArrayList<RDInfo>();
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            //normalize, so that text doesn't get parsed into separate nodes
            doc.getDocumentElement().normalize();
            //System.out.println(doc);
            //System.out.println("Root element: " + doc.getDocumentElement().getNodeName());
            //Node ns = doc.getDocumentElement().getFirstChild();
            NodeList nList = doc.getDocumentElement().getChildNodes();

            for (int i = 0; i < nList.getLength(); i++) {
                Node node = nList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element eNode = (Element) node;
                    String table = eNode.getNodeName();
                    String aus = eNode.getAttributes().getNamedItem("actual_used_size").getTextContent();
                    int actual_used_size = Integer.parseInt(aus);
                    String tfs = eNode.getAttributes().getNamedItem("totalFieldSize").getTextContent();
                    int totalFieldSize = Integer.parseInt(tfs);
                    String filename = eNode.getElementsByTagName("filename").item(0).getTextContent();
                    int loc = Integer.parseInt(eNode.getElementsByTagName("loc").item(0).getTextContent());
                    Statement stm = new Statement(loc, filename);
                    String unused_field = eNode.getElementsByTagName("unused_field").item(0).getTextContent();
                    RDInfo rdinfo = new RDInfo(totalFieldSize, actual_used_size, stm, unused_field, table);
                    rdinfo.self_print();
                    rdinfos.add(rdinfo);
                }

            }
        }catch (Exception ex) {
            System.out.println("Weird stuff happening..." + ex);
            Messages.showMessageDialog("Weird stuff happening..." + ex, "Info Box", Messages.getInformationIcon());
            return null;
        }

        return rdinfos;
    }

    public static List<LoopInfo> extractLIInfo(File file){
        List<LoopInfo> loopInfo = new ArrayList<LoopInfo>();
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            //normalize, so that text doesn't get parsed into separate nodes
            doc.getDocumentElement().normalize();
            //System.out.println(doc);
            System.out.println("Root element: " + doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName("loopInvariant");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                System.out.println("\nCurrent Element :" + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    //System.out.println("Staff id : " + eElement.getAttribute("id"));
                    String start = eElement.getElementsByTagName("start").item(0).getTextContent();
                    System.out.println("Loop Start: " + start);
                    int rstart = Integer.parseInt(start);
                    String startFilename = eElement.getElementsByTagName("startFilename").item(0).getTextContent();
                    System.out.println("Loop startFilename: " + startFilename);
                    String statement = eElement.getElementsByTagName("statement").item(0).getTextContent();
                    int rstatement = Integer.parseInt(statement);
                    System.out.println("Statement to Change: " + statement);
                    String filename = eElement.getElementsByTagName("filename").item(0).getTextContent();
                    System.out.println("File name: " + filename);
                    LoopInfo lf = new LoopInfo(rstart, startFilename, rstatement, filename);
                    loopInfo.add(lf);


                }
            }

        System.out.println("LISIZE" + loopInfo.size());
        }catch (Exception ex) {
            System.out.println("Weird stuff happening..." + ex);
            return null;
        }

        return loopInfo;
    }
    public static List<IRInfo> extractIRInfo(File file){
        List<IRInfo> irfs = new ArrayList<IRInfo>();

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            //normalize, so that text doesn't get parsed into separate nodes
            doc.getDocumentElement().normalize();
            //System.out.println(doc);
            System.out.println("Root element: " + doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName("inefficientRender");

            //LinkedList<DSInfo> dsInfo = new LinkedList<DSInfo>();

            for (int temp = 0; temp < nList.getLength(); temp++) {
                IRInfo irf = new IRInfo();
                Node nNode = nList.item(temp);

                System.out.println("\nCurrent Element :" + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    //System.out.println("Staff id : " + eElement.getAttribute("id"));
                    String tstatement = eElement.getElementsByTagName("statement").item(0).getTextContent();
                    String tempstate = tstatement.replaceAll("\\s+", "");
                    int rstate = Integer.parseInt(tempstate);
                    System.out.println("Statement: " + rstate);
                    String tfilename = eElement.getElementsByTagName("filename").item(0).getTextContent();
                    String filename = tfilename.replaceAll("\\s+", "");
                    System.out.println("File name: " + filename);
                    irf.setFilename(filename);
                    irf.setStatement(rstate);
                    irfs.add(irf);


                    //loopInfo.add(dsf);


                }
            }

        }catch (Exception ex) {
            System.out.println("Weird stuff happening..." + ex);
            Messages.showMessageDialog("Weird stuff happening..." + ex, "Info Box", Messages.getInformationIcon());
            return null;
        }
        return irfs;
    }
    public static List<String> extractFiles(List<RDInfo> rdinfos){
        List<String> fs = new ArrayList<String>();
        for(int i = 0; i < rdinfos.size(); i++){
            RDInfo rdinfo = rdinfos.get(i);
            String f = rdinfo.getStm().getFilename();
            if(!fs.contains(f)) {
                fs.add(f);
            }
        }
        return fs;
    }
    public static List<String> constructCSFilePanel(List<CSInfo> csinfos){
        List<String> fs = new ArrayList<String>();
        for(int i = 0; i < csinfos.size(); i++){
            CSInfo csinfo = csinfos.get(i);
            for(int j = 0; j < csinfo.getExprs().size(); j ++ ){
                String f = csinfo.getExprs().get(j).getFilename();
                if(!fs.contains(f)) {
                    fs.add(f);
                }
            }
        }
        return fs;
    }
    public static List<JButton> constructFilesPanel(List<String> fs){

        List<JButton> labels = new ArrayList<JButton>();
        for(int i = 0; i < fs.size(); i ++){
            String f = fs.get(i);
            JButton fLabel = new JButton(f.replace(System.getProperty("line.separator"), ""));
            fLabel.setPreferredSize(new Dimension(400, 20));
            labels.add(fLabel);
        }
        System.out.println("Label size: " + labels.size());
        return labels;
    }
    public static List<String> constructLIFilePanel(List<LoopInfo> loopInfos){
        List<String> fs = new ArrayList<String>();
        for(int i = 0; i < loopInfos.size(); i++){
            LoopInfo loopInfo = loopInfos.get(i);
            String f = loopInfo.getFilename();
            if(!fs.contains(f)) {
                fs.add(f);

            }
        }
        return fs;
    }
    public static List<String> constructIRFilePanel(List<IRInfo> irInfos){
        List<String> fs = new ArrayList<String>();
        for(int i = 0; i < irInfos.size(); i++){
            IRInfo irInfo = irInfos.get(i);
            String f = irInfo.getFilename();
            if(!fs.contains(f)) {
                fs.add(f);
            }
        }
        return fs;
    }
    public static List<String> constructDSFilePanel(List<DSInfo> dsInfos){
        List<String> fs = new ArrayList<String>();
        for(int i = 0; i < dsInfos.size(); i++){
            DSInfo dsInfo = dsInfos.get(i);
            String f = dsInfo.getFilename1();
            if(!fs.contains(f)) {
                fs.add(f);
                System.out.println("DSFILE NAME :" + f);
            }
        }
        System.out.println("DSFS SIZE :" + fs.size());
        return fs;
    }
    public static JPanel constructFiles(List<JButton> labels){
        JPanel result = new JPanel();
        result.setPreferredSize(new Dimension(420, 600));
        result.setLayout(new FlowLayout());
        result.setAutoscrolls(true);
        //result.setBorder(BorderFactory.createLineBorder(Color.black));
        for(int i = 0; i < labels.size(); i ++){
            JButton f = labels.get(i);
            result.add(f);
        }

        return result;
    }
    public static void removeHighlighters(Project project){
        for (Pair<Editor, EditorMouseMotionListener> pair : editorListeners){
            try {
                pair.getKey().removeEditorMouseMotionListener(pair.getValue());
            } catch (Throwable e) {

            }

        }
        editorListeners = new ArrayList<Pair<Editor, EditorMouseMotionListener>>();
        Editor e = FileEditorManager.getInstance(project).getSelectedTextEditor();
        e.getMarkupModel().removeAllHighlighters();
    }
    public static String getRP(Project project){
        String results_path;
        String lib = "/";
        String root  = "static-analyzer/";
        IdeaPluginDescriptor plugin = PluginManager.getPlugin(PluginId.getId("com.intellij.powerstation"));
        String powerstation_path = plugin.getPath().toString();
        // get the version of the plugin in order to get the snapshot's name
        String version = plugin.getVersion();
        String name = plugin.getName();
        String snapshot = name + "-" + version + ".jar";

        String[] jarnames = powerstation_path.split("/");
        String jarname = jarnames[jarnames.length - 1];
        powerstation_path = powerstation_path.replace(jarname, "");
        System.out.println("path of powerstation is " + powerstation_path);
        System.out.println("snapshot name is :" + snapshot);

        String path_ana = powerstation_path + lib + root; //static-analyzer/analyze.sh
        String applications = path_ana + "/applications";

        String basedir = project.getBasePath();
        String input = basedir;
        String[] appnames = input.split("/");
        String app = "PW-" + appnames[appnames.length - 1];
        results_path =  applications + "/" + app + "/results/";

        return results_path;
    }
}
